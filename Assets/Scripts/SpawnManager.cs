﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour
{
    private float startDelay = 3;
    private float spawnInterval = 1.5f;
    private float min = 1.5f;
    private float max = 3f;
    private int nextScoreBarrier = 1000;
    public int level = 0;
    public GameObject seagull;
    public GameObject pelican;
    public GameObject invincibilityPowerup;
    public GameObject damagePowerup;
    public GameObject[] clouds;
    private PlayerController playerControllerScript;
    public TextMeshProUGUI titleText;
    private AudioSource spawnAudio;
    public AudioClip enemySpawnSound;


    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        spawnAudio = GetComponent<AudioSource>();
    }

    public void StartGame(int difficulty)
    {
        playerControllerScript.gameOver = false;
        playerControllerScript.ScaleModelTo(Vector3.one);
        playerControllerScript.scoreText.enabled = true;
        playerControllerScript.healthText.enabled = true;
        playerControllerScript.fireParticleRight.SetActive(true);
        playerControllerScript.fireParticleLeft.SetActive(true);
        playerControllerScript.GetComponent<BoxCollider>().enabled = true;
        GameObject.Find("DifficultyButton").SetActive(false);
        titleText.enabled = false;
        StartCoroutine(SpawnEnemy());
        InvokeRepeating("SpawnCloud",startDelay, spawnInterval*5);
        StartCoroutine(SpawnPowerup());
        // Difficulty determines the start speed of enemies
        level = difficulty;
    }

    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    // Spawns powerups
    IEnumerator SpawnPowerup() {
        while(!playerControllerScript.gameOver){
            yield return new WaitForSeconds(Random.Range(10,20));
            Vector3 spawnPos = new Vector3(0,Random.Range(1,9),9);
            int roll = Random.Range(1,3);
            switch (roll){
                case 1:
                    Instantiate(invincibilityPowerup, spawnPos, invincibilityPowerup.transform.rotation);
                    break;
                case 2:
                    Instantiate(damagePowerup, spawnPos, damagePowerup.transform.rotation);
                    break;
            }
        }
    }

// Spawn enemies
    IEnumerator SpawnEnemy() {
        while (!playerControllerScript.gameOver) {
            yield return new WaitForSeconds(Random.Range(min,max));
            Vector3 spawnPos = new Vector3(0,Random.Range(1,9),11);
            int roll = Random.Range(1,10);
            // Pelicans spawn more rarely than gulls
            if (roll < 3) {
                Instantiate(pelican, spawnPos, pelican.transform.rotation);
            }
            else {
                Instantiate(seagull, spawnPos, seagull.transform.rotation);
            }
            // Wing flap plays when an enemy spawns
            spawnAudio.PlayOneShot(enemySpawnSound, 0.5f);

            // Increase spawn rate based on score (decrease min and max seconds between spawns)
            if (playerControllerScript.score > nextScoreBarrier) {
                min -= 0.1f;
                if (min <= 0) {
                    min = 0.1f;
                }
                max -= 0.5f;
                if (max <= 0) {
                    max = 0.1f;
                }
                // Intervals between increases in difficulty get longer
                nextScoreBarrier *= 2;
                level ++;
            }
        }
    }

// Spawns a random cloud in the background
    void SpawnCloud() {
        if (!playerControllerScript.gameOver) {
            Vector3 spawnPos = new Vector3(Random.Range(-2,-10),Random.Range(1,9),15);
            int roll = Random.Range(0,3);
            switch(roll) {
                case 0:
                    Instantiate(clouds[0], spawnPos, clouds[0].transform.rotation);
                    break;
                case 1:
                    Instantiate(clouds[1], spawnPos, clouds[0].transform.rotation);
                    break;
                case 2:
                    Instantiate(clouds[2], spawnPos, clouds[0].transform.rotation);
                    break;
            }
        }
    }
}
