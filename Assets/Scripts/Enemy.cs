﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Enemy : MonoBehaviour
{

    public int health;
    public int value;
    private AudioSource enemyAudio;
    public AudioClip deathSound;
    public GameObject model;
    public GameObject feathers;
    private PlayerController playerControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        enemyAudio = GetComponent<AudioSource>();
        playerControllerScript =  GameObject.Find("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Projectile")) {
            health -= playerControllerScript.projectileDamage;
            if(health <= 0) {
                playerControllerScript.UpdateScore(value);
                Instantiate(feathers, transform.position, feathers.transform.rotation);
                enemyAudio.PlayOneShot(deathSound, 1.0f);
                // Scale model to zero and destroy the collider rather than the object to allow audio to play
                // The object will still be destroyed when it goes too far off screen
                ScaleModelTo(Vector3.zero);
                Destroy(GetComponent<BoxCollider>());
            }
            Destroy(other.gameObject);
        }
    }

    private void ScaleModelTo(Vector3 scale)
        {
            model.transform.localScale = scale;
        }
}
