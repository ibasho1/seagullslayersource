﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRight : MonoBehaviour
{

    public float speed;
    private Rigidbody projectileRb;

    // Start is called before the first frame update
    void Start()
    {
        projectileRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        if(gameObject.transform.position.z > 9){
            Destroy(gameObject);
        }
    }

    // Destroys projectiles when they hit the right bound 
    // Prevents the player from killing enemies not yet on screen, especially with the damage powerup
    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("RightWall")) {
            Destroy(gameObject);
        }
    }
}
