﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class RestartButton : MonoBehaviour
{

    private Button button;
    private SpawnManager spawnManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Restart);
        spawnManagerScript = GameObject.Find("Spawn Manager").GetComponent<SpawnManager>();
    }

    void Restart() {
        spawnManagerScript.RestartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
