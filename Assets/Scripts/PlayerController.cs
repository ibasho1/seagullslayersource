﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI; 

public class PlayerController : MonoBehaviour
{

    // Primatives
    public float speed;
    // Controls flash rate
    private float invincibilityDeltaTime = 0.2f;
    public int health = 5;
    public int score = 0;
    public int projectileDamage = 1;
    public bool gameOver = true;
    private bool isInvincible = false;
    private bool damageUp = false;

    // Objects
    private Rigidbody playerRb;
    private Vector3 direction;
    public ParticleSystem explosionParticle;
    public GameObject model;
    public GameObject fireParticleLeft;
    public GameObject fireParticleRight;
    public GameObject shield;
    public GameObject projectile;
    public GameObject restartButton;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI scoreText; 
    public TextMeshProUGUI finalScoreText; 
    public TextMeshProUGUI tryAgainText; 
    private Coroutine invincibilityCoroutine;
    private Coroutine damageCoroutine;
    private AudioSource playerAudio;
    public AudioClip harmSound;
    public AudioClip fireSound;
    public AudioClip powerFireSound;
    public AudioClip deathSound;
    public AudioClip powerupSound;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.enabled = false;
        healthText.enabled = false;
        ScaleModelTo(Vector3.zero);
        playerRb = GetComponent<Rigidbody>();
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Get input and create direction
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        direction = new Vector3(0,verticalInput,horizontalInput);
        // Update health - if less than 0, set to 0 to prevent negative number being displayed
        if (health < 0) {
            health = 0;
        }
        healthText.SetText("Health: " + health);
        //Debug.Log("Health: " + health);
        // Fire projectile
        if (Input.GetKeyDown(KeyCode.Space)) {
            Instantiate(projectile, transform.position, projectile.transform.rotation);
            if (damageUp) {
                playerAudio.PlayOneShot(powerFireSound, 0.2f);
            }
            else {
                playerAudio.PlayOneShot(fireSound, 0.5f);
            }
        }
    }

    void FixedUpdate()
    {
        if (!gameOver){
            //Move the player
            MovePlayer(direction);
        }
    }

    public void UpdateScore(int value) {
        if (!gameOver){
            // Increment score with the point value of the enemy
            score += value;
            // Update score text
            scoreText.SetText("Score: " + score.ToString("000000"));
        }
    }

// Move player in the input direction at a certain speed
    void MovePlayer(Vector3 direction) {
        if (!gameOver) {
            playerRb.velocity = direction * speed;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (!gameOver) {
            ReactToCollision(other);
        }
    }

    private void ReactToCollision(Collider other) {
        switch (other.tag)
        {
            case "Seagull": 
                if (!isInvincible && !gameOver){
                    health--;
                    playerAudio.PlayOneShot(harmSound, 0.5f);
                    invincibilityCoroutine = StartCoroutine(Invincibility(2,0));
                }
                break;
            case "Pelican":
                if (!isInvincible && !gameOver){
                    health -= 2;
                    playerAudio.PlayOneShot(harmSound, 0.5f);
                    invincibilityCoroutine = StartCoroutine(Invincibility(2,0));
                }
                break;
            case "InvincibilityPowerup":
                if (!gameOver){
                    playerAudio.PlayOneShot(powerupSound, 0.1f);
                    Destroy(other.gameObject);
                    if (isInvincible) {
                        // Stop running invincibility coroutine to prevent bugs caused by overlap
                        StopCoroutine(invincibilityCoroutine);
                    }
                    invincibilityCoroutine = StartCoroutine(Invincibility(10,1));
                }
                break;
            case "DamagePowerup":
                if (!gameOver){
                    playerAudio.PlayOneShot(powerupSound, 0.3f);
                    Destroy(other.gameObject);
                    if (damageUp) {
                        // Stop running damage coroutine to prevent bugs caused by overlap
                        StopCoroutine(damageCoroutine);
                    }
                    damageCoroutine = StartCoroutine(Damage());
                }
                break;
            default: break;
        }
        
        if (health <= 0) {
            if (isInvincible) {
                    StopCoroutine(invincibilityCoroutine);
            }
            GameOver();
        }
    }

    // Temporary invincibility or invincibilty powerup
    private IEnumerator Invincibility(float duration, int type)
    {
        // Reset the model in case the player picks up a powerup while the model is scaled to zero
        ScaleModelTo(Vector3.one);
        switch(type) {
            // Temporary invincibilty when hit by an enemy
            case 0:
                isInvincible = true;
                // 'Flashes' model by scaling it to zero and one alternately
                for (float i = 0; i < duration; i += invincibilityDeltaTime)
                {
                    if (model.transform.localScale == Vector3.one)
                    {
                        ScaleModelTo(Vector3.zero);
                    }
                    else
                    {
                        ScaleModelTo(Vector3.one);
                    }
                    yield return new WaitForSeconds(invincibilityDeltaTime);
                }
                isInvincible = false;
                break;
            case 1:
                isInvincible = true;
                // Shows a yellow shield around the player
                shield.SetActive(true);
                yield return new WaitForSeconds(duration);
                isInvincible = false;
                shield.SetActive(false);
                break;
        }
    }

    // Damage powerup
    private IEnumerator Damage() {
        damageUp = true;
        projectileDamage = 3;
        yield return new WaitForSeconds(10);
        projectileDamage = 1;
        damageUp = false;

    }

    // Ends Game
    public void GameOver () {
        gameOver = true;
        
        ScaleModelTo(Vector3.zero);
        GetComponent<BoxCollider>().enabled = false;
        finalScoreText.SetText("FINAL SCORE: " + score.ToString("000000"));
        finalScoreText.enabled = true;
        tryAgainText.enabled = true;
        restartButton.SetActive(true);
        fireParticleLeft.SetActive(false);
        fireParticleRight.SetActive(false);
        playerAudio.PlayOneShot(deathSound, 0.5f);
        Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
    }

    // Scales the player model
    public void ScaleModelTo(Vector3 scale)
    {
        model.transform.localScale = scale;
    }
}
