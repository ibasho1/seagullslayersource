﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class BeginButton : MonoBehaviour
{
    private Button button;
    private SpawnManager spawnManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Begin);
        spawnManagerScript = GameObject.Find("Spawn Manager").GetComponent<SpawnManager>();
    }

    void Begin()
    {
        // Hide button
        gameObject.SetActive(false);
        spawnManagerScript.StartGame(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
