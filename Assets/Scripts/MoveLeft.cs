﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{

    public float speed;
    private SpawnManager spawnManagerScript;

    // Start is called before the first frame update
    void Start()
    {
        spawnManagerScript =  GameObject.Find("Spawn Manager").GetComponent<SpawnManager>();
        // Increase enemy speed as the player progresses
        if (gameObject.CompareTag("Seagull") || gameObject.CompareTag("Pelican")){
            speed += spawnManagerScript.level/2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        if(gameObject.transform.position.z < -15){
            Destroy(gameObject);
        }
    }
}
