# Seagull Slayer
Seagull Slayer is a simple arcade-style game created in Unity.
<br />
You play as a robotic crow on a mission to take down some very mean-looking seagulls and pelicans.
<br />
The arrow keys are used to move and the space bar is used to fire.
<br />
<br />

Below is a link to the final build:
https://k00249492.github.io/SeagullSlayer/
